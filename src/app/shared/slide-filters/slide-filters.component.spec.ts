import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatAutocompleteModule, MatIconModule, MatInputModule, MatSidenavModule,
  MatSliderModule
} from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SlideFiltersComponent } from './slide-filters.component';
import { ContextService } from '../context.service';

import { HttpClientModule } from '@angular/common/http';
describe('HeaderComponent', () => {
  let component: SlideFiltersComponent;
  let fixture: ComponentFixture<SlideFiltersComponent>;
  let context: ContextService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
             declarations: [ SlideFiltersComponent ],
             providers   : [ ContextService ],
             imports     : [
               BrowserAnimationsModule,
               FormsModule,
               ReactiveFormsModule,
               RouterTestingModule,
               MatAutocompleteModule,
               MatIconModule,
               MatInputModule,
               MatSidenavModule,
               MatSliderModule,
               HttpClientModule,
             ]
           })
           .compileComponents();
    context = TestBed.get(ContextService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => { 
    localStorage.removeItem('firstset');
});


  it('should create', () => {
    expect(component)
      .toBeTruthy();
  });

  it('check if we are able to set localstorage default value on first run on client system ', () => {
    if(localStorage.getItem('firstset') === null){
      localStorage.setItem('firstset', 'true');
      const val = localStorage.getItem('firstset');
      expect(val).toBe('true');
    }

  });

    
  it('check if video count increasing in shared context service', () => {
    context.videosCountPerPage.next(25);
    context.videosCountPerPage.subscribe((count) => {
      expect(count).toEqual(25);
    });
  });

});
