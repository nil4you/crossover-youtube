import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { appConfig } from 'appConfig';
import { ICountryListModel } from '@shared/models/country-list.interface';
import { ICategoryListInterface } from '@shared/models/category-list.interface';
import { ContextService } from '@shared/context.service';

import { YoutubeService } from '../../modules/youtube/service/youtube.service';

@Component({
  selector   : 'app-slide-filters',
  templateUrl: './slide-filters.component.html',
  styleUrls  : [ './slide-filters.component.scss' ]
})
export class SlideFiltersComponent implements OnInit {


  public countryFormControl: FormControl = new FormControl();
  public countryList: ICountryListModel[] = appConfig.countryList;

  @Output() public filterSlideOpen:EventEmitter<any> =  new EventEmitter();

  public categoryFormControl: FormControl = new FormControl();
  // public categoriesList: ICategoryListInterface[] = [
  //   {name: 'Film & Animation', id: 1},
  //   {name: 'Autos & Vehicles', id: 2},
  //   {name: 'Music', id: 10},
  //   {name: 'Pets & Animals', id: 4}
  // ];
  public categoriesList:Array<ICategoryListInterface>;


  public defaultVideosOnPage: number = appConfig.maxVideosToLoad;

  constructor(private appContext: ContextService, private youtubeService: YoutubeService ) {
  }

  public ngOnInit() {
  this.youtubeService.getCategoryList()
    .subscribe((data) => {
      this.categoriesList = data;
      this.setDefaults();
    });

  }

  changeCountry(country){
    localStorage.setItem('defaultCountry', country);
    this.youtubeService.reload.next(country);
    //this.youtubeService.filterUpdate(country);
  }

  changeCategory(category){
    localStorage.setItem('defaultCategory', category);
   this.youtubeService.reload.next(category);
    //this.youtubeService.filterUpdate(category);
    //changeCategory
  }

  closeToggle(){
    this.filterSlideOpen.emit('true');
  }
  public onChangeVideosPerPage(count: number) {
    this.appContext.videosCountPerPage.next(count);
    localStorage.setItem('defaultVideosOnPage', count.toString());
  }

  private async setDefaults() {
    let defaultCountry , defaultCategory
    if(localStorage.getItem('defaultCountry') === null){
      defaultCountry = this.countryList.find((country) =>
      country.code === appConfig.defaultRegion);
      localStorage.setItem('defaultCountry', defaultCountry.code);

      localStorage.setItem('defaultVideosOnPage', this.defaultVideosOnPage.toString());
      defaultCategory = this.categoriesList.find((data) => {
        return (data.id == 10)
      });
      localStorage.setItem('defaultCategory', defaultCategory.id);
    } else {
      defaultCountry = await this.countryList.find((data) => {
        return (data.code == localStorage.getItem('defaultCountry'))
      });
      localStorage.getItem('defaultCountry');

      this.defaultVideosOnPage = parseInt(localStorage.getItem('defaultVideosOnPage'));
      defaultCategory = await this.categoriesList.find((data) => {
        return (data.id == parseInt(localStorage.getItem('defaultCategory')))
      });
    }
    this.countryFormControl.setValue(defaultCountry.name);
    this.categoryFormControl.setValue(defaultCategory.name);
    }

  }
