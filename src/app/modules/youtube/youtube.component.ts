import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { catchError, take, tap, map } from 'rxjs/internal/operators';
import { throwError } from 'rxjs/index';
import * as Rx from 'rxjs';
import { YoutubeService } from './service/youtube.service';
import { ContextService } from '@shared/context.service';
import { VideoClass } from './models/video.class';

@Component({
  selector   : 'app-youtube-component',
  templateUrl: './youtube.component.html',
  styleUrls  : [ './youtube.component.scss' ]
})

export class YoutubeComponent implements OnInit {
  public trendingVideos= new Rx.BehaviorSubject<VideoClass[]>([]);
  public appendtrendingVideos: Observable<VideoClass[]>;
  public loadingError$ = new Subject<boolean>();
  public videos: VideoClass[];
  public count: number;
  public page: number = 1;
  public oldData= new Rx.BehaviorSubject([]);

  constructor(private youtubeService: YoutubeService,
              private appContext: ContextService) {
                
  }

  public ngOnInit(): void {
    this.appContext.moduleTitle.next('YOUTUBE');
    this.loadVideos();
    this.appContext.videosCountPerPage.subscribe((count) => {
      this.loadVideos(count);
      this.count = count;
    });
  }

  private reset(value){
    this.trendingVideos= new Rx.BehaviorSubject<VideoClass[]>([]);
    this.page = 1;
    this.oldData= new Rx.BehaviorSubject([]);
    this.loadVideos();
  }

  private loadVideos(videosPerPage?: number) {
      this.youtubeService.getTrendingVideos(videosPerPage)
      .pipe(
        tap( (data)=> {
          let tdata;
          this.trendingVideos.subscribe(data => tdata=data);
          
          const newData = tdata.concat(data);
          this.trendingVideos.next(newData);
          this.oldData = newData;
          this.page++;
          const scroll = this.youtubeService.getScroll();
          if(scroll>0) {
            setTimeout(()=>{
              document.getElementById("youtube-contaier").scrollTop = scroll;
            },200)
          }
        
         }) ,
        map(
          data => this.trendingVideos.next(data)
        ),
        catchError((error: any) => {
          this.loadingError$.next(true);
          return throwError(error);
        })
      )
      .subscribe(data => console.log(data));
    

  }

  onScroll(event){
    this.youtubeService.setScroll(event.currentScrollPosition);
    this.youtubeService.getTrendingVideos(this.count, this.page)
                              .pipe(
                                take(1),
                                tap( (data)=> {
                                  let tdata;
                                  this.trendingVideos.subscribe(data => tdata=data);
                                  
                                  const newData = tdata.concat(data);
                                  this.trendingVideos.next(newData);
                                  this.oldData = newData;
                                  this.page++;
                                 }) ,
                                catchError((error: any) => {
                                  this.loadingError$.next(true);
                                  return throwError(error);
                                })
                              ).subscribe();
                              
  }

}
