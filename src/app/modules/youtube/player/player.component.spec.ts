import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PlayerComponent } from './player.component';
import { YoutubeService } from '../service/youtube.service';
import { HttpClientModule } from '@angular/common/http';

describe('PlayerComponent', () => {
  let component: PlayerComponent;
  let fixture: ComponentFixture<PlayerComponent>;
  let youtube: YoutubeService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
             declarations: [ PlayerComponent ],
             imports     : [ RouterTestingModule, HttpClientModule ],
             providers   : [
              YoutubeService
            ]
           })
           .compileComponents();
           
           youtube = TestBed.get(YoutubeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component)
      .toBeTruthy();
  });

  it('should validate video', () => {
    youtube.validateVideo('gl1aHhXnN1k')
    .subscribe((data)=> {
      expect(parseInt(data.items.length))
      .toBeGreaterThan(0);
    });
  });

});
