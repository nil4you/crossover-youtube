import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { appConfig } from 'appConfig';
import { YoutubeService } from '../service/youtube.service';
import {Router} from '@angular/router';

@Component({
  selector   : 'app-player',
  templateUrl: './player.component.html',
  styleUrls  : [ './player.component.scss' ]
})
export class PlayerComponent implements OnInit {
  constructor(public sanitizer: DomSanitizer,
    private youtubeService: YoutubeService,
    private router: Router){
    
  }

  public embedUrl: string;
  public videoLoader: boolean=true;

  public ngOnInit() {
    const id = window.location.href
                     .replace(/^.*\//g, '')
                     .replace(/^.*\..*/g, '');

    if (!id.length) {
      return;
    }

    this.youtubeService.validateVideo(id)
      .subscribe((data)=> {
        if(data.items.length) {
          this.embedUrl = appConfig.getYoutubeEmbdedUrl(id);
          this.videoLoader = false;
        }else {
          this.router.navigateByUrl('/youtube');
        }
      });
    
  }

  /* On video ready hide loader */
  public loadVideo(): void {
    this.videoLoader = false;
  }

}
