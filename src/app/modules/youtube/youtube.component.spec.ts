import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component, EventEmitter, Input } from '@angular/core';
import { MatButtonModule, MatIconModule, MatSidenavModule } from '@angular/material';
import { MomentModule } from 'ngx-moment';

import { YoutubeComponent } from './youtube.component';
import { YoutubeService } from './service/youtube.service';
import { ContextService } from '@shared/context.service';
import { Observable } from 'rxjs';
import 'rxjs-compat/add/observable/of';
import { VideoClass } from '@modules/youtube/models/video.class';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
 import 'rxjs/Rx';
 import { HttpClientModule } from '@angular/common/http';
 import {BehaviorSubject} from 'rxjs/BehaviorSubject';   
 
import { catchError, take, tap, map } from 'rxjs/internal/operators';

@Component({
  selector: 'app-video-component',
  template: ''
})
class VideoComponent {
  @Input() public video: VideoClass;
}

describe('YoutubeComponent', () => {
  let component: YoutubeComponent;
  let fixture: ComponentFixture<YoutubeComponent>;
  
  let youtube: YoutubeService;
  const service = {
    getTrendingVideos() {
      //return true;
      return Observable.of([]);
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
             declarations: [ YoutubeComponent, VideoComponent ],
             imports     : [
              HttpClientModule,
               RouterTestingModule,
               MatButtonModule,
               MatIconModule,
               MatSidenavModule,
               MomentModule,
               InfiniteScrollModule,
               
             ],
             providers   : [
               {provide: YoutubeService, useValue: service},
               ContextService,
             ]
           })
           .compileComponents();
           youtube = TestBed.get(YoutubeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component)
      .toBeTruthy();
  });

  it('should load videos on scroll', () => {
    let spy = spyOn(component, "onScroll").and.returnValue(false);
    component.onScroll('event');
    expect(component.onScroll).toHaveBeenCalled();
    //component.scrolled();
  }); 

//  return c.promise;   
});
