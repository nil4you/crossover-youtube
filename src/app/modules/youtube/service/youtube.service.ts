import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError, tap, } from 'rxjs/internal/operators';

import * as Rx from 'rxjs';
import { appConfig } from 'appConfig';
import { VideoClass } from '../models/video.class';
import { ICategoryListInterface } from '@shared/models/category-list.interface';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';   

@Injectable({
  providedIn:'root'
})
export class YoutubeService {
  token:string = null;
  oldData=new Array();
  scrolled:number = 0;
  filter:string;
  public reload: BehaviorSubject<any> = new BehaviorSubject<any>(0);
  
  public trendingVideos= new Rx.BehaviorSubject<VideoClass[]>([]);
  constructor(private http: HttpClient) {
    this.reload.subscribe(
      value => {
        this.token=null; 
        this.scrolled=0;
        this.oldData=new Array();
      }
    );
  }


  public setScroll(amount){
    this.scrolled=amount;
  }

  
  public getScroll(){
    return this.scrolled;
  }

  public getCategoryList(){
    const params: any = {
      part           : 'snippet',
      regionCode     : appConfig.defaultRegion,
      key            : appConfig.youtubeApiKey
    };
    return this.http.get<any>(appConfig.getYoutubeEndPoint('videoCategories'), {params})
      .pipe(map(
        (data) => data.items
                      .map(<ICategoryListInterface>(item) =>  ({'id':item.id, 'name':item.snippet.title}))
                      .filter((item) => item.id !== '')
      ),
      catchError(this.handleError('getCategoryList'))
    )
  }

  public filterUpdate(filter){
  //  this.reload.next(filter);
   // return of(filter);
  }

  public getTrendingVideos(videosPerPage?: number, page?: number): Observable<VideoClass[]> {
    if(localStorage.getItem('defaultVideosOnPage') !== null && videosPerPage === undefined) {
      videosPerPage = parseInt(localStorage.getItem('defaultVideosOnPage'));
    }
    videosPerPage = (videosPerPage>50) ? 50: videosPerPage;
    const params: any = {
      part           : appConfig.partsToLoad,
      chart          : appConfig.chart,
      videoCategoryId: (localStorage.getItem('defaultCategory')) ? localStorage.getItem('defaultCategory') : appConfig.defaultCategoryId,
      regionCode     : (localStorage.getItem('defaultCountry')) ? localStorage.getItem('defaultCountry') : appConfig.defaultRegion,
      maxResults     : videosPerPage ? videosPerPage : (appConfig.maxVideosToLoad>50)?50:appConfig.maxVideosToLoad,
      key            : appConfig.youtubeApiKey
    };

    if(page == undefined && this.oldData.length){
      return of(this.flatten(this.oldData));
    }
    if(this.token != null && page != undefined){
      params.pageToken = this.token;
    }
    
    return this.http.get<any>(appConfig.getYoutubeEndPoint('videos'), {params})
               .pipe(
                 tap((data)=>{
                  this.token = data.nextPageToken;
                   this.oldData.push(data.items
                    .map((item) => new VideoClass(item))
                    .filter((item) => item.id !== ''));
                 }
                 ),
                 map(
                   (data) => data.items
                                 .map((item) => new VideoClass(item))
                                 .filter((item) => item.id !== '')
                 ),
                 catchError(this.handleError('getTrendingVideos'))
               ) as Observable<VideoClass[]>;

  }

  public validateVideo(id) {
//https://www.googleapis.com/youtube/v3/videos?part=id&id=b_bJQgZdjzo&key={YOUR_API_KEY}
return this.http.get<any>(appConfig.getYoutubeValidateUrl(id));
//appConfig.getYoutubeValidateUrl(id);
//getYoutubeValidateUrl
  }

  private handleError(operation: string = 'operation') {
    return (error: any) => {
      error.operation = operation;
      return throwError(error);
    };
  }

  private  flatten(input) {
    const stack = [...input];
    const res = [];
    while (stack.length) {
      // pop value from stack
      const next = stack.pop();
      if (Array.isArray(next)) {
        // push back array items, won't modify the original input
        stack.push(...next);
      } else {
        res.push(next);
      }
    }
    //reverse to restore input order
    return res.reverse();
  }
}
